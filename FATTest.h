//
// Created by fifal on 4.1.17.
//

#ifndef ZOS_FATTEST_H
#define ZOS_FATTEST_H


#include <cstdint>
#include "FATStruct.h"
#include <cstdio>
#include <cstring>
#include <string>

class FATTest {
    static const int32_t FAT_SIMULATED_ERROR = INT32_MAX - 5;
public:
    static void create_test_fat(std::string name, int8_t fat_type, int8_t fat_copies, int16_t cluster_size, int32_t usable_cluster_count);
    static void print_fat_table(int32_t *fat_table, int32_t size);
    static void simulate_fat_errors(int32_t * fat_table, int32_t size, int max_errors);
};


#endif //ZOS_FATTEST_H
