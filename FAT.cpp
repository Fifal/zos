//
// Created by fifal on 21.12.16.
//

#include <algorithm>
#include <unistd.h>
#include "FAT.h"

/**
 * Default constructor
 */
FAT::FAT(std::string file_path) {
    this->file_path = file_path;
    directories = std::vector<FATStruct::directory_data>();
    clusters = std::vector<FATStruct::cluster_data>();

    //Memsets
    memset(br.volume_descriptor, '\0', sizeof(br.volume_descriptor));
    memset(br.signature, '\0', sizeof(br.signature));

    if (parse_data(file_path) == 1) {
        std::cout << "Chyba při načítání dat." << std::endl;
        exit(1);
    }
}

//------------------------------------------------------
//------------------------------------------------------
//                READ AND WRITE STUFF                 -
//------------------------------------------------------
//------------------------------------------------------

/**
 * Loads boot record into structure
 * @param file
 * @return
 */
int FAT::load_boot_record(FILE *file) {
    /* Parse boot_record to move file pointer */
    struct FATStruct::boot_record *boot_record = (struct FATStruct::boot_record *) malloc(
            sizeof(struct FATStruct::boot_record));
    fread(boot_record, sizeof(struct FATStruct::boot_record), 1, file);
    br.usable_cluster_count = boot_record->usable_cluster_count;
    br.cluster_size = boot_record->cluster_size;
    br.fat_copies = boot_record->fat_copies;
    br.fat_type = boot_record->fat_type;
    strcpy(br.signature, boot_record->signature);
    strcpy(br.volume_descriptor, boot_record->volume_descriptor);

    free(boot_record);
    return 0;
}

/**
 * Parse data from file
 * @param file_path path to file
 * @return 0 if successful, 1 otherwise
 */
int FAT::parse_data(std::string file_path) {

    FILE *file = fopen(file_path.c_str(), "r");
    if (file == NULL) {
        return 1;
    }

    if (load_boot_record(file) != 0) {
        return 1;
    };

    /* Load fat table */
    fat_table = (int32_t **) (malloc(sizeof(int32_t *) * br.fat_copies));
    for (int32_t i = 0; i < br.fat_copies; ++i) {
        fat_table[i] = (int32_t *) (malloc(sizeof(int32_t) * br.usable_cluster_count));
        fread(fat_table[i], sizeof(*fat_table[i]) * br.usable_cluster_count, 1, file);
    }

    long start_of_content = ftell(file);

    /* Check if fat table contains errors */
    int errors = check_tables_for_errors();
    if (errors != 0) {
        if (repair_table(errors) == 1) {
            std::cout << "Nepodařilo se obnovit FAT tabulku." << std::endl;
            return 1;
        } else {
            std::cout << "Tabulka byla opravena." << std::endl;
            for (int i = 1; i < br.fat_copies; ++i) {
                for (int j = 0; j < br.usable_cluster_count; ++j) {
                    fat_table[i][j] = fat_table[0][j];
                }
            }
        }
    }

    /* Calculate max number of subdirs in one dir */
    size_t max_dirs_in_cluster = br.cluster_size / sizeof(FATStruct::directory);

    for (int j = 0; j < br.usable_cluster_count; ++j) {
        size_t offset = start_of_content + (br.cluster_size * j);
        fseek(file, offset, SEEK_SET);

        /* If it is FAT Directory */
        if (fat_table[0][j] == FAT_DIRECTORY) {
            for (int i = 0; i < max_dirs_in_cluster; ++i) {
                struct FATStruct::directory *directory = (struct FATStruct::directory *) malloc(
                        sizeof(struct FATStruct::directory));
                fread(directory, sizeof(FATStruct::directory), 1, file);
                if (directory->start_cluster != 0) {
                    struct FATStruct::directory_data dir_data;
                    dir_data.dir = directory;
                    dir_data.super_directory = j;
                    directories.push_back(dir_data);
                }
            }
        }
    }

    std::string cluster_string;
    FATStruct::cluster_data data;

    for (int k = 0; k < directories.size(); ++k) {
        if (directories.at(k).dir->is_file) {
            int32_t index = directories.at(k).dir->start_cluster;
            int32_t index_value;
            while (true) {
                index_value = fat_table[0][index];

                size_t offset = start_of_content + (br.cluster_size * index);
                fseek(file, offset, SEEK_SET);

                char *cluster = (char *) malloc(sizeof(char) * br.cluster_size);
                fread(cluster, (size_t) br.cluster_size, 1, file);
                cluster_string.append(cluster);
                data.clusters.push_back(index);

                if (index_value == FAT_FILE_END) {
                    data.data = cluster_string;
                    data.start_cluster = directories.at(k).dir->start_cluster;
                    data.end_cluster = 0;
                    cluster_string = "";
                    clusters.push_back(data);
                    data.clusters.clear();
                    break;
                } else {
                    if (fat_table[0][index] != FAT_FILE_END) {
                        index = fat_table[0][index];
                    }
                }
            }
        }
    }
    fclose(file);
    return 0;
}

/**
 * Checks loaded fat tables for errors
 * @return 0 if there are no errors, -1 if fat contains simulated error, index of cyclic error start
 */
int FAT::check_tables_for_errors() {
    int result = 0;

    for (int i = 0; i < br.fat_copies; ++i) {
        for (int j = 0; j < br.usable_cluster_count; ++j) {
            if (fat_table[i][j] == FAT_SIMULATED_ERROR) {
                result = -1;
            }
        }
    }

    /* Check for cyclic table */
    for (int k = 0; k < br.usable_cluster_count; ++k) {
        int32_t index = fat_table[0][k];
        int32_t prev_index = k;
        int32_t count = 0;

        while (true) {
            // If index is file end, unused, bad cluster or dir continue...
            if (index == FAT_FILE_END || index == FAT_UNUSED || index == FAT_BAD_CLUSTER || index == FAT_DIRECTORY) {
                break;
            }

            // If index > usable cluster count -> out of table return index of error
            if (index > br.usable_cluster_count) {
                return prev_index;
            }

            // If index < prev index -> cycle
            if (index < prev_index) {
                return prev_index;
            }

            if (index == FAT_DIRECTORY) {
                return k + count;
            } else {
                if (index == FAT_UNUSED) {
                    return prev_index;
                }

                prev_index = index;
                index = fat_table[0][index];

                if (index == FAT_UNUSED || index == FAT_DIRECTORY || index == FAT_BAD_CLUSTER) {
//                    if (count == 0) {
                    return k + count;
//                    } else {
//                        return k + count;
//                    }
                }
            }
            count++;
        }
    }


    return result;
}

/**
 * Repairs broken table
 * //TODO: načítat dál i bez tabulky, podle toho zjistim kde jsou jaký soubory a podle toho obnovit tabulky
 * @return 0 if successful, 1 otherwise
 */
int FAT::repair_table(int error) {
    if (error == -1) {
        for (int i = 0; i < br.fat_copies; ++i) {
            for (int j = 0; j < br.usable_cluster_count; ++j) {
                if (fat_table[i][j] == FAT_SIMULATED_ERROR) {
                    int32_t repaired_value = find_correct_value(j);
                    if (repaired_value == -1) {
                        std::cout << "Nepodařilo se nalézt správnou hodnotu pro index " << j << std::endl;
                    } else {
                        std::cout << "Opravuji fat_table[" << i << "][" << j << "] = " << fat_table[i][j]
                                  << " => fat_table[" << i << "][" << j << "] = " << repaired_value << std::endl;
                        fat_table[i][j] = repaired_value;
                    }
                }
            }
        }
    } else {
        std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
        std::cout << "Nalezena chyba ve fat tabulce[0][" << error << "]" << ": " << fat_table[0][error] << std::endl
                  << std::endl;
        if (error >= 1 && error <= br.usable_cluster_count) {
            std::cout << "Vypisuji hodnoty fat copies pro danou chybu:" << std::endl;

            for (int j = error - 1; j <= error + 1; ++j) {
                for (int i = 0; i < br.fat_copies; ++i) {
                    std::cout << "fat_table[" << i << "][" << j << "]=" << fat_table[i][j] << ";\t";
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;

            for (int i = 1; i < br.fat_copies; ++i) {
                std::cout << "Opravuji hodnotou fat_table[" << i << "][" << error << "]=" << fat_table[i][error]
                          << std::endl;
                if (fat_table[0][error] == fat_table[i][error]) {
                    return 1;
                }

                fat_table[0][error] = fat_table[i][error];
                std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
            }
            int32_t more_errors = check_tables_for_errors();
            if (more_errors != 0) {
                return repair_table(more_errors);
            } else {
                return 0;
            }
        }
    }
}

/**
 * Finds correct value in fat table copies
 * @param fat_table_index index
 * @return correct value if successful, -1 otherwise
 */
int32_t FAT::find_correct_value(int32_t fat_table_index) {
    for (int i = 0; i < br.fat_copies; ++i) {
        if (fat_table[i][fat_table_index] != FAT_SIMULATED_ERROR) {
            return fat_table[i][fat_table_index];
        }
    }
    return -1;
}

/**
 * Writes data to output fiĺe
 * @return 0 if successful, 1 otherwise
 */
int FAT::write_to_file() {
    return write_to_file(file_path);
}

/**
 * Writes data to output fiĺe
 * @param file_path path to file
 * @return 0 if successful, 1 otherwise
 */
int FAT::write_to_file(std::string file_path) {
    FILE *file = fopen(file_path.c_str(), "w");

    if (file == NULL) {
        return 1;
    }

    // At first write BOOT RECORD
    fwrite(&br, sizeof(br), 1, file);

    // Now br.fat_copies times fat_table
    for (int i = 0; i < br.fat_copies; ++i) {
        for (int j = 0; j < br.usable_cluster_count; ++j) {
            fwrite(&fat_table[i][j], sizeof(*fat_table[i]), 1, file);
        }
    }

    directories = Util::sort_vector(directories);
    clusters = Util::sort_vector(clusters);

    //TODO: špatně počít... pokud jedu, dojedu na cluster 34 a teď mi příjde, že mám dát do clusteru 29, tak se
    //      posunu na začátek 29. ale tam už něco může být...
    //TODO: SNAD OK, pomocí řazení
    long content_start = ftell(file);
    int32_t current_cluster_pointer = 0;
    size_t position_in_cluster = 0;
    for (int k = 0; k < directories.size(); ++k) {
        FATStruct::directory_data directory_data = directories.at(k);

        // If dir has parent dir in current cluster
        if (directory_data.super_directory == current_cluster_pointer) {
            fwrite(directory_data.dir, sizeof(FATStruct::directory), 1, file);
            position_in_cluster += sizeof(FATStruct::directory);

            // Shouldn't happen
            assert(position_in_cluster < br.cluster_size);
        } else {
            // If it is not in current cluster, find cluster in which it belongs
            int32_t cluster = directory_data.super_directory;
            size_t offset = (size_t) (br.cluster_size * (cluster - current_cluster_pointer)) - position_in_cluster;
            position_in_cluster = 0;

            // Move pointer in file to that cluster
            fseek(file, offset, SEEK_CUR);
            current_cluster_pointer = cluster;

            // Write
            fwrite(directory_data.dir, sizeof(FATStruct::directory), 1, file);
            position_in_cluster += sizeof(FATStruct::directory);

            // Shouldn't happen
            assert(position_in_cluster < br.cluster_size);
        }
    }

    // Move to start of another cluster
    size_t offset = br.cluster_size - position_in_cluster;
    position_in_cluster = 0;
    fseek(file, offset, SEEK_CUR);
    current_cluster_pointer++;

    // Clusters with some data
    for (int l = 0; l < clusters.size(); ++l) {
        char *data = (char *) malloc(sizeof(char) * br.cluster_size);
        int data_pointer = 0;

        for (int i = 0; i < clusters.at(l).clusters.size(); ++i) {
            memset(data, '\0', (size_t) br.cluster_size);
            strncpy(data, clusters.at(l).data.c_str() + data_pointer, br.cluster_size - 1);

            int32_t cluster = clusters.at(l).clusters.at(i);
            // Move to cluster
            int32_t offset = (size_t) (br.cluster_size * (cluster - current_cluster_pointer));
            fseek(file, offset, SEEK_CUR);
            current_cluster_pointer = cluster;

            // Write data
            fwrite(data, (size_t) br.cluster_size, 1, file);

            data_pointer += br.cluster_size - 1;
            current_cluster_pointer++;
        }
        free(data);
    }

    assert(position_in_cluster == 0);
    fseek(file, content_start, SEEK_SET);
    current_cluster_pointer = 0;

    char *empty_cluster = (char *) malloc(sizeof(char) * br.cluster_size);
    memset(empty_cluster, '\0', (size_t) br.cluster_size);

    /* List through whole fat table */
    for (int m = 0; m < br.usable_cluster_count; m++) {
        /* If there is unused or bad cluster on index m write empty cluster */
        if (fat_table[0][m] == FAT_UNUSED || fat_table[0][m] == FAT_BAD_CLUSTER) {
            /* Move to cluster m in file */
            size_t offset = (size_t) (br.cluster_size) * (m - current_cluster_pointer);
            fseek(file, offset, SEEK_CUR);

            fwrite(empty_cluster, (size_t) br.cluster_size, 1, file);

            /* FWRITE MOVES BY ONE CLUSTER, M + 1!!! */
            current_cluster_pointer = m + 1;
        }
    }

    free(empty_cluster);
    fclose(file);
}
//------------------------------------------------------

//------------------------------------------------------
//                    FUNCTIONS                        -
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//-------------------- #7 print ------------------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Prints whole pseudoFAT system
 */
void FAT::print_fat() {
    std::vector<FATStruct::directory_data> root_children = get_children(0);
    if (root_children.size() == 0) {
        printf("EMPTY");
    } else {
        printf("+ROOT\n");
        for (int i = 0; i < root_children.size(); ++i) {
            print_dir(1, root_children.at(i));
        }
        printf("--\n");
    }
}

/**
 * Prints one directory
 * @param indention_level level of indention
 */
void FAT::print_dir(int indention_level, FATStruct::directory_data dir_data) {
    std::string indention = "";
    for (int i = 0; i < indention_level; ++i) {
        indention.append("\t");
    }

    if (dir_data.dir->is_file) {
        printf("%s-%s %d %d\n", indention.c_str(), dir_data.dir->name, dir_data.dir->start_cluster,
               get_file_cluster_count(dir_data.dir->start_cluster));
    } else {
        printf("%s+%s\n", indention.c_str(), dir_data.dir->name);
        std::vector<FATStruct::directory_data> children = get_children(dir_data.dir->start_cluster);
        for (int i = 0; i < children.size(); ++i) {
            print_dir(indention_level + 1, children.at(i));
        }
        printf("%s--\n", indention.c_str());
    }

}

/**
 * Returns Vector of childs in parrent directory
 * @param parent
 * @return
 */
std::vector<FATStruct::directory_data> FAT::get_children(int32_t parent) {
    std::vector<FATStruct::directory_data> children = std::vector<FATStruct::directory_data>();
    for (int i = 0; i < directories.size(); ++i) {
        FATStruct::directory_data dir_data = directories.at(i);
        if (dir_data.super_directory == parent) {
            children.push_back(dir_data);
        }
    }
    return children;
}

/**
 * Returns cluster count of file which starts at start_cluster
 * @param start_cluster start cluster
 * @return int32_t cluster count
 */
int32_t FAT::get_file_cluster_count(int32_t start_cluster) {
    for (int i = 0; i < clusters.size(); ++i) {
        FATStruct::cluster_data cl_data = clusters.at(i);
        if (cl_data.start_cluster == start_cluster) {
            return (int32_t) cl_data.clusters.size();
        }
    }
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//-------------- #6 print file content -----------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Prints file content from filesystem
 * @param file_path file path in fs
 */
void FAT::print_file_content(std::string file_path) {
    FATStruct::cluster_data cluster_data = get_file_content(0, 0, file_path);
    FATStruct::directory_data file_header = find_dir_by_path(file_path, 0, 0);

    if (cluster_data.clusters.size() == 0 && file_header.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
    } else {
        std::cout << file_header.dir->name << ": " << cluster_data.data; //<< std::endl;
    }
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//--------------- #5 delete empty dir ------------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Deletes dir from file system
 * @param dir_path path to dir in filesystem
 */
void FAT::delete_dir(std::string dir_path) {
    FATStruct::directory_data dir = find_dir_by_path(dir_path, 0, 0);
    if (dir.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }
    std::vector<FATStruct::directory_data> dir_content;

    if (dir.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    dir_content = get_children(dir.dir->start_cluster);

    if (dir_content.size() != 0) {
        std::cout << "PATH NOT EMPTY" << std::endl;
        return;
    }

    directories.erase(
            std::remove_if(directories.begin(), directories.end(), [&](FATStruct::directory_data const &dir_data) {
                return dir_data.dir == dir.dir;
            }),
            directories.end()
    );

    write_to_file();

    std::cout << "OK" << std::endl;
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//--------------- #4 create empty dir ------------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Creates new dir in fat dir_path
 * @param dir_name name of directory
 * @param dir_path path in fat
 */
void FAT::make_new_dir(std::string dir_name, std::string dir_path) {
    if (dir_name.find("/") != -1) {
        std::cout << "SLASH IN DIRECTORY NAME NOT ALLOWED" << std::endl;
        return;
    }

    FATStruct::directory_data dir_in_fat = find_dir_by_path(dir_path, 0, 0);
    std::vector<FATStruct::directory_data> children;

    if (dir_in_fat.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    children = get_children(dir_in_fat.dir->start_cluster);

    if (children.size() > 9) {
        std::cout << "NOT ENOUGH SPACE FOR SUB FOLDER" << std::endl;
        return;
    }

    std::vector<int32_t> free_cluster = find_free_clusters(1);

    assert(free_cluster.size() == 1);

    FATStruct::directory *dir_data = (struct FATStruct::directory *) malloc(
            sizeof(struct FATStruct::directory));

    dir_data->is_file = false;
    dir_data->size = 0;
    dir_data->start_cluster = free_cluster.at(0);
    memset(dir_data->name, '\0', sizeof(dir_data->name));
    strcpy(dir_data->name, dir_name.c_str());

    FATStruct::directory_data dir_header;
    dir_header.super_directory = dir_in_fat.dir->start_cluster;
    dir_header.dir = dir_data;

    directories.push_back(dir_header);

    assert(free_cluster.at(0) < br.usable_cluster_count);
    fat_table[0][free_cluster.at(0)] = FAT_DIRECTORY;

    write_to_file();

    std::cout << "OK" << std::endl;
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//-------------- #3 print file clusters ----------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Prints clusters in which has file content
 * @param fat_file_path path to file in pseudoFAT
 */
void FAT::print_file_clusters(std::string fat_file_path) {
    FATStruct::cluster_data cluster_data = get_file_content(0, 0, fat_file_path);
    FATStruct::directory_data directory_data = get_file_by_start_cluster(cluster_data.start_cluster);

    if (cluster_data.clusters.size() > 0 && directory_data.dir != NULL) {
        std::cout << directory_data.dir->name << " ";
        for (int j = 0; j < cluster_data.clusters.size(); ++j) {
            std::cout << cluster_data.clusters.at(j) << ", ";
        }
        std::cout << std::endl;
    } else {
        std::cout << "PATH NOT FOUND" << std::endl;
    }
}

/**
 * Recursively find path of file and if file exists prints his clusters
 * @param parent in which directory we are currently in
 * @param level current level
 * @param path file path
 */
FATStruct::cluster_data FAT::get_file_content(int32_t parent, int level, std::string path) {
    std::vector<FATStruct::directory_data> children = get_children(parent);
    std::vector<std::string> split_path = Util::split(path, "/");
    FATStruct::cluster_data cluster_data;

    for (int i = 0; i < children.size(); ++i) {
        // If part of path in current level exists -> move deeper
        if (strcmp(children.at(i).dir->name, split_path.at(level).c_str()) == 0 && level < split_path.size() - 1) {
            return get_file_content(children.at(i).dir->start_cluster, level + 1, path);
        }

            // If we are at the deepest level
        else if (level == split_path.size() - 1) {

            // If we have found our file
            if (strcmp(children.at(i).dir->name, split_path.at(level).c_str()) == 0) {
                cluster_data = find_file_content(children.at(i));
            }
        }
    }

    return cluster_data;
}

/**
 * Finds content of FATStruct::directory_data
 * @param directory_data directory
 * @return FATStruct::cluster_data with data of directory if sucessful, otherwise returns empty struct
 */
struct FATStruct::cluster_data FAT::find_file_content(FATStruct::directory_data directory_data) {
    for (int i = 0; i < clusters.size(); ++i) {
        if (clusters.at(i).start_cluster == directory_data.dir->start_cluster) {
            return clusters.at(i);
        }
    }
    FATStruct::cluster_data cl_data;
    return cl_data;
}

/**
 * Returns directory data for file on posion of start_cluster
 * @param start_cluster start cluster of the file
 * @return directory data
 */
struct FATStruct::directory_data FAT::get_file_by_start_cluster(int32_t start_cluster) {
    FATStruct::directory_data directory_data;
    for (int i = 0; i < directories.size(); ++i) {
        if (directories.at(i).dir->start_cluster == start_cluster) {
            return directories.at(i);
        }
    }
    return directory_data;
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//-------------- #2 delete file from fs ----------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Deletes file from file system
 * @param fat_file_path path to file in file system
 */
void FAT::delete_file_from_fs(std::string fat_file_path) {
    FATStruct::directory_data file_header = find_dir_by_path(fat_file_path, 0, 0);

    if (file_header.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    FATStruct::cluster_data file_data = find_file_content(file_header);

    if (file_header.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    assert(file_data.start_cluster != 0);

    delete_file_clusters_from_fat_table(file_data.clusters);

    clusters.erase(
            std::remove_if(clusters.begin(), clusters.end(), [&](FATStruct::cluster_data const &cl_data) {
                return cl_data.start_cluster == file_data.start_cluster;
            }),
            clusters.end());

    //TODO: free();
    directories.erase(
            std::remove_if(directories.begin(), directories.end(), [&](FATStruct::directory_data const &dir_data) {
                return dir_data.dir == file_header.dir;
            }),
            directories.end()
    );

    free(file_header.dir);

    write_to_file();

    std::cout << "OK" << std::endl;
}

/**
 * Deletes file content clusters from fat table
 * @param clusters file content clusters
 */
void FAT::delete_file_clusters_from_fat_table(std::vector<int32_t> clusters) {
    for(int j = 0; j < br.fat_copies; j++) {
        for (int i = 0; i < clusters.size(); ++i) {
            assert(clusters.at(i) < br.usable_cluster_count);

            fat_table[j][clusters.at(i)] = FAT_UNUSED;
        }
    }
}
//------------------------------------------------------

//------------------------------------------------------
//------------------------------------------------------
//-------------- #1 load file into fs ------------------
//------------------------------------------------------
//------------------------------------------------------
/**
 * Loads file into dir
 * @param file_path path to existing file
 * @param dir path to dir in pseudoFAT
 */
void FAT::load_file_into_fs(std::string file_path, std::string dir) {
    std::vector<std::string> split_path = Util::split(file_path, "/");
    std::string file_name = split_path.at(split_path.size() - 1);
    std::string file_content = Util::get_file_content(file_path);

    if (file_name.find("/") != -1) {
        std::cout << "SLASH IN FILE NAME NOT ALLOWED" << std::endl;
        return;
    }

    if (file_content.compare("") == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }

    FATStruct::directory_data parent_directory = find_dir_by_path(dir, 0, 0);

    if (parent_directory.super_directory == -1) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    if (get_children(parent_directory.dir->start_cluster).size() == 10) {
        std::cout << "NOT ENOUGH MEMORY IN DIR" << std::endl;
        return;
    }

    /* How many free_clusters does content takes */
    int32_t cluster_size = (int32_t) (ceil((double) file_content.size() / (double) br.cluster_size));
    std::vector<int32_t> free_clusters = find_free_consecutive_clusters(cluster_size);

    /* If we have found start_cluster -> file will be fragmented */
    if (free_clusters.size() == 0) {
        free_clusters = find_free_clusters(cluster_size);
        return;
    }

    if (free_clusters.size() == 0) {
        std::cout << "NOT ENOUGH MEMORY" << std::endl;
        return;
    }


    /* Set structure data and push into vectors */
    FATStruct::directory *file_header_data = (struct FATStruct::directory *) malloc(
            sizeof(struct FATStruct::directory));
    file_header_data->start_cluster = free_clusters.at(0);
    file_header_data->size = (int32_t) file_content.size();
    file_header_data->is_file = true;
    memset(file_header_data->name, '\0', sizeof(file_header_data->name));
    strcpy(file_header_data->name, file_name.c_str());

    FATStruct::directory_data file_header;
    file_header.super_directory = parent_directory.dir->start_cluster;
    file_header.dir = file_header_data;

    directories.push_back(file_header);

    FATStruct::cluster_data file_data;
    file_data.data = file_content;
    file_data.clusters = free_clusters;
    file_data.start_cluster = free_clusters.at(0);
    file_data.end_cluster = free_clusters.at(free_clusters.size() - 1);

    clusters.push_back(file_data);

    /* Write clusters into fat table */
    write_file_clusters_to_fat_table(free_clusters);

    write_to_file();

    std::cout << "OK" << std::endl;
}

/**
 * Finds directory in pseudoFAT
 * @param dir_path path to directory
 * @return returns directory if found, otherwise returns struct with dir.super_directory = -1;
 */
struct FATStruct::directory_data FAT::find_dir_by_path(std::string dir_path, int32_t parent, int iteration) {
    std::vector<std::string> split = Util::split(dir_path, "/");
    std::vector<FATStruct::directory_data> root = get_children(parent);

    if (dir_path == "/") {
        FATStruct::directory *root_dir = (struct FATStruct::directory *) malloc(
                sizeof(struct FATStruct::directory));;
        root_dir->start_cluster = 0;
        root_dir->is_file = false;
        memset(root_dir->name, '\0', sizeof(root_dir->name));
        strcpy(root_dir->name, "/");
        root_dir->size = 0;

        FATStruct::directory_data root;
        root.super_directory = 0;
        root.dir = root_dir;

        return root;
    }

    FATStruct::directory_data result;
    result.super_directory = -1;

    for (int i = 0; i < root.size(); ++i) {
        if (iteration < split.size() - 1) {
            if (strcmp(root.at(i).dir->name, split.at(iteration).c_str()) == 0) {
                return find_dir_by_path(dir_path, root.at(i).dir->start_cluster, iteration + 1);
            }
        } else {
            if (strcmp(root.at(i).dir->name, split.at(iteration).c_str()) == 0) {
                return root.at(i);
            }
        }
    }
    return result;
}


/**
 * Returns vector of fragmented clusters
 * @param number_of_clusters how many free clusters do we want
 * @return vector<int32_t> returns vector of clusters if successful, returns empty vector otherwise
 */
std::vector<int32_t> FAT::find_free_consecutive_clusters(int32_t number_of_clusters) {
    std::vector<int32_t> clusters = std::vector<int32_t>();

    for (int32_t i = 0; i < br.usable_cluster_count; ++i) {
        if (fat_table[0][i] == FAT_UNUSED) {
            while (fat_table[0][i] == FAT_UNUSED) {
                clusters.push_back(i);
                i++;
                if (clusters.size() >= number_of_clusters) {
                    return clusters;
                }
            }
        }
        clusters = std::vector<int32_t>();
    }

    clusters = std::vector<int32_t>();
    return clusters;
}

/**
 * Finds number_of_clusters in FAT table which has FAT_UNUSED flag and returns vector of those
 * @param number_of_clusters how many free clusters do we want
 * @return returns vector<int32_t> with cluster indexes if sucessful, returns NULL otherwise
 */
std::vector<int32_t> FAT::find_free_clusters(int32_t number_of_clusters) {
    std::vector<int32_t> free_clusters = std::vector<int32_t>();

    for (int32_t i = 0; i < br.usable_cluster_count; ++i) {
        if (fat_table[0][i] == FAT_UNUSED) {
            free_clusters.push_back(i);

            if (free_clusters.size() == number_of_clusters) {
                return free_clusters;
            }
        }
    }

    return std::vector<int32_t>();
}

/**
 * Writes file clusters into FAT table
 * @param clusters file content clusters
 */
void FAT::write_file_clusters_to_fat_table(std::vector<int32_t> clusters) {
    for (int i = 0; i < br.fat_copies; ++i) {
        for (int j = 0; j < clusters.size(); ++j) {
            int32_t cluster = clusters.at(j);
            if (cluster < br.usable_cluster_count && j < clusters.size() - 1) {
                fat_table[i][cluster] = cluster + 1;
            } else if (cluster < br.usable_cluster_count && j == clusters.size() - 1) {
                fat_table[i][cluster] = FAT_FILE_END;
            }
        }

    }
}
//------------------------------------------------------


//------------------------------------------------------
//------------------------------------------------------
//                      GETTERS                        -
//------------------------------------------------------
//------------------------------------------------------
struct FATStruct::boot_record FAT::get_boot_record() {
    return br;
}

std::vector<FATStruct::directory_data> FAT::get_directories() {
    return directories;
}

std::vector<FATStruct::cluster_data> FAT::get_clusters() {
    return clusters;
}

int32_t **FAT::get_fat_table() {
    return fat_table;
}
//------------------------------------------------------
FAT::~FAT() {
    for (int i = 0; i < directories.size(); ++i) {
        free(directories.at(i).dir);
    }
    for (int j = 0; j < br.fat_copies; ++j) {
        free(fat_table[j]);
    }
    free(fat_table);
}