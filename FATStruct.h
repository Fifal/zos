//
// Created by fifal on 22.12.16.
//

#ifndef ZOS_FATSTRUCT_H
#define ZOS_FATSTRUCT_H


#include <cstdint>
#include <vector>
#include <string>

class FATStruct {
public:
    struct boot_record {
        char volume_descriptor[250];
        int8_t fat_type;
        int8_t fat_copies;
        int16_t cluster_size;
        int32_t usable_cluster_count;
        char signature[9];
    };//272B

    struct directory {
        char name[13];
        bool is_file;
        int32_t size;
        int32_t start_cluster;
    };//24B

    struct directory_data{
        struct directory *dir;
        int32_t super_directory;
    };

    struct cluster_data{
        std::string data;
        int32_t start_cluster;
        int32_t end_cluster;
        std::vector<int32_t> clusters;
    };
};


#endif //ZOS_FATSTRUCT_H
